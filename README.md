<div class="alert alert-block alert-info">
Calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9.<br/>
Print calculated value with precision = 2
</div>